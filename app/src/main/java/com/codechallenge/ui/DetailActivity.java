package com.codechallenge.ui;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.codechallenge.CodeChallengeApplication;
import com.codechallenge.R;
import com.codechallenge.di.component.ApplicationComponent;
import com.codechallenge.di.component.DaggerDetailActivityComponent;
import com.codechallenge.di.component.DetailActivityComponent;
import com.codechallenge.di.qualifier.ApplicationContext;
import com.codechallenge.model.MovieDetails;
import com.codechallenge.retrofit.APIInterface;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends BaseActivity {

    @Inject
    public APIInterface apiInterface;
    @Inject
    @ApplicationContext
    public Context mContext;
    DetailActivityComponent detailActivityComponent;

    private ImageView coverImage;
    private TextView titleText;
    private TextView yearText;
    private TextView ratingText;
    private TextView synopsisText;
    private TextView categoriesText;
    private TextView scoreText;
    private TextView reviewsText;
    private TextView popularityText;
    private TextView directorText;
    private TextView writerText;
    private TextView actorsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        hideSearch();
        setTitle(getString(R.string.movie_details_text));
        viewsRegister();

        String imdbID = getIntent().getStringExtra("imdbID");

        ApplicationComponent applicationComponent = CodeChallengeApplication.get(this).getApplicationComponent();
        detailActivityComponent = DaggerDetailActivityComponent.builder()
                .applicationComponent(applicationComponent)
                .build();

        detailActivityComponent.inject(this);

        callApi(imdbID);
    }

    private void callApi(String imdbID) {
        showProgress();
        apiInterface.getMovieDetails(imdbID).enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("True")) {
                    MovieDetails films = response.body();
                    updateUI(films);
                } else {
                    Toast.makeText(DetailActivity.this, response.body().getError(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {
                hideProgress();
                Toast.makeText(DetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void viewsRegister() {
        coverImage = findViewById(R.id.coverImage);
        titleText = findViewById(R.id.titleText);
        yearText = findViewById(R.id.yearText);
        ratingText = findViewById(R.id.ratingText);
        categoriesText = findViewById(R.id.categoriesText);
        synopsisText = findViewById(R.id.synopsisText);
        scoreText = findViewById(R.id.scoreText);
        reviewsText = findViewById(R.id.reviewsText);
        popularityText = findViewById(R.id.popularityText);
        directorText = findViewById(R.id.directorText);
        writerText = findViewById(R.id.writerText);
        actorsText = findViewById(R.id.actorsText);
    }

    private void updateUI(MovieDetails films) {
        Glide.
                with(this).
                load(films.getPoster()).
                placeholder(R.drawable.ic_image_placeholder).
                into(coverImage);

        titleText.setText(String.format(getString(R.string.formatter_string), films.getTitle()));
        yearText.setText(String.format(getString(R.string.formatter_string), films.getYear()));
        ratingText.setText(films.getImdbRating());
        categoriesText.setText(String.format(getString(R.string.formatter_string), films.getRuntime()));
        synopsisText.setText(String.format(getString(R.string.formatter_string), films.getPlot()));
        scoreText.setText(films.getImdbRating());
        reviewsText.setText(films.getImdbVotes());
        popularityText.setText(films.getMetascore());
        directorText.setText(String.format(getString(R.string.formatter_string), films.getDirector()));
        writerText.setText(String.format(getString(R.string.formatter_string), films.getWriter()));
        actorsText.setText(String.format(getString(R.string.formatter_string), films.getActors()));
    }
}

