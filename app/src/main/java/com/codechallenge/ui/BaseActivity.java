package com.codechallenge.ui;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;

import com.codechallenge.R;

public class BaseActivity extends AppCompatActivity {

    protected ViewGroup fullView;
    protected Toolbar toolbar;
    protected SearchView searchView;
    protected ContentLoadingProgressBar progressBar;
    protected Window window;
    protected int apiLevel = 0;
    protected FrameLayout frameLayout;

    @SuppressLint({"RestrictedApi", "SourceLockedOrientationActivity"})
    @Override
    public void setContentView(int layoutResID) {
        try {
            apiLevel = android.os.Build.VERSION.SDK_INT;
            if (apiLevel > 20) {
                window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }

            fullView = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_base, null);
            frameLayout = fullView.findViewById(R.id.activity_content_base);
            getLayoutInflater().inflate(layoutResID, frameLayout, true);
            super.setContentView(fullView);
            progressBar = findViewById(R.id.progressBarBase);
            toolbar = findViewById(R.id.toolbarBase);
            searchView = toolbar.findViewById(R.id.searchView);
            searchView.setIconifiedByDefault(false);
            searchView.setQueryHint(getString(R.string.search_here_text));

            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void showBackButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public void hideBackButton() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
    }

    public void showSearch() {
        searchView.setVisibility(View.VISIBLE);
    }

    public void hideSearch() {
        searchView.setVisibility(View.GONE);
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public void setTitle(String text) {
        getSupportActionBar().setTitle(text);
    }
}

