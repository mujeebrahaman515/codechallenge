package com.codechallenge.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.codechallenge.CodeChallengeApplication;
import com.codechallenge.R;
import com.codechallenge.adapter.RecyclerViewAdapter;
import com.codechallenge.di.component.ApplicationComponent;
import com.codechallenge.di.component.DaggerMainActivityComponent;
import com.codechallenge.di.component.MainActivityComponent;
import com.codechallenge.di.modules.MainActivityContextModule;
import com.codechallenge.di.qualifier.ActivityContext;
import com.codechallenge.di.qualifier.ApplicationContext;
import com.codechallenge.model.MoviesList;
import com.codechallenge.model.Search;
import com.codechallenge.retrofit.APIInterface;
import com.codechallenge.utils.PaginationScrollListener;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements RecyclerViewAdapter.ClickListener {

    @Inject
    public RecyclerViewAdapter recyclerViewAdapter;
    @Inject
    public APIInterface apiInterface;
    @Inject
    @ApplicationContext
    public Context mContext;
    @Inject
    @ActivityContext
    public Context activityContext;
    MainActivityComponent mainActivityComponent;
    private RecyclerView recyclerView;
    private String searchText;
    private int mPageNum = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hideBackButton();
        recyclerView = findViewById(R.id.recyclerView);
        GridLayoutManager layoutManager = new GridLayoutManager(MainActivity.this, 2);
        recyclerView.setLayoutManager(layoutManager);

        ApplicationComponent applicationComponent = CodeChallengeApplication.get(this).getApplicationComponent();
        mainActivityComponent = DaggerMainActivityComponent.builder()
                .mainActivityContextModule(new MainActivityContextModule(this))
                .applicationComponent(applicationComponent)
                .build();

        mainActivityComponent.injectMainActivity(this);
        recyclerView.setAdapter(recyclerViewAdapter);

        recyclerView
                .setOnScrollListener(new PaginationScrollListener(recyclerView, layoutManager) {
                    @Override
                    protected void loadMoreItems(int pageNum) {
                        mPageNum = pageNum;
                        callApi();
                    }

                    @Override
                    public int listSize() {
                        return recyclerViewAdapter.getData().size();
                    }
                });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (query.trim().length() > 0) {
                    searchText = query;
                    mPageNum = 1;
                    callApi();
                } else {
                    Toast.makeText(MainActivity.this, "Search cannot be empty", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void callApi() {
        if (mPageNum == 1) {
            recyclerViewAdapter.clearData();
        }
        showProgress();
        apiInterface.getMoviesList(searchText, mPageNum + "").enqueue(new Callback<MoviesList>() {
            @Override
            public void onResponse(Call<MoviesList> call, Response<MoviesList> response) {
                hideProgress();
                if (response.body().getResponse().equalsIgnoreCase("True")) {
                    recyclerViewAdapter.setData(response.body().getSearch());
                } else {
                    Toast.makeText(MainActivity.this, response.body().getError(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MoviesList> call, Throwable t) {
                hideProgress();
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(Search item) {
        startActivity(new Intent(activityContext, DetailActivity.class).putExtra("imdbID", item.getImdbID()));
    }
}
