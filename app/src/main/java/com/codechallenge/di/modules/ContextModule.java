package com.codechallenge.di.modules;

import android.content.Context;

import com.codechallenge.di.qualifier.ApplicationContext;
import com.codechallenge.di.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context provideContext() {
        return context;
    }
}