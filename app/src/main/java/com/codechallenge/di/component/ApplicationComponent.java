package com.codechallenge.di.component;

import android.content.Context;

import com.codechallenge.CodeChallengeApplication;
import com.codechallenge.di.modules.ContextModule;
import com.codechallenge.di.modules.RetrofitModule;
import com.codechallenge.di.qualifier.ApplicationContext;
import com.codechallenge.di.scopes.ApplicationScope;
import com.codechallenge.retrofit.APIInterface;

import dagger.Component;

@ApplicationScope
@Component(modules = {ContextModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    APIInterface getApiInterface();

    @ApplicationContext
    Context getContext();

    void injectApplication(CodeChallengeApplication myApplication);
}


