package com.codechallenge.di.component;

import android.content.Context;

import com.codechallenge.ui.MainActivity;
import com.codechallenge.di.modules.AdapterModule;
import com.codechallenge.di.qualifier.ActivityContext;
import com.codechallenge.di.scopes.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(modules = AdapterModule.class, dependencies = ApplicationComponent.class)
public interface MainActivityComponent {

    @ActivityContext
    Context getContext();

    void injectMainActivity(MainActivity mainActivity);
}

