package com.codechallenge.di.component;

import com.codechallenge.di.scopes.ActivityScope;
import com.codechallenge.ui.DetailActivity;

import dagger.Component;

@Component(dependencies = ApplicationComponent.class)
@ActivityScope
public interface DetailActivityComponent {

    void inject(DetailActivity detailActivity);
}

