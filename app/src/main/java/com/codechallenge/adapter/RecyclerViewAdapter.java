package com.codechallenge.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.codechallenge.R;
import com.codechallenge.model.Search;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private List<Search> data;
    private RecyclerViewAdapter.ClickListener clickListener;

    @Inject
    public RecyclerViewAdapter(ClickListener clickListener) {
        this.clickListener = clickListener;
        data = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Search itemData = data.get(position);
        holder.titleText.setText(itemData.getTitle());
        holder.yearText.setText(itemData.getYear());
        Glide.
                with(holder.coverImage.getContext()).
                load(itemData.getPoster()).
                placeholder(R.drawable.ic_image_placeholder).
                into(holder.coverImage);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<Search> getData() {
        return this.data;
    }

    public void setData(List<Search> data) {
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void clearData() {
        this.data.clear();
        notifyDataSetChanged();
    }

    public interface ClickListener {
        void onItemClick(Search item);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView titleText;
        private TextView yearText;
        private ImageView coverImage;
        private ViewGroup constraintLayoutContainer;

        ViewHolder(View itemView) {
            super(itemView);

            titleText = itemView.findViewById(R.id.titleText);
            yearText = itemView.findViewById(R.id.yearText);
            coverImage = itemView.findViewById(R.id.coverImage);
            constraintLayoutContainer = itemView.findViewById(R.id.constraintLayout);

            constraintLayoutContainer.setOnClickListener(v -> {
                if (clickListener != null) {
                    clickListener.onItemClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}

