package com.codechallenge.utils;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    private RecyclerView mRecyclerView;

    private GridLayoutManager layoutManager;
    // indicates the current page which Pagination is fetching.
    private int currentPage = 0;

    protected PaginationScrollListener(RecyclerView recyclerView, GridLayoutManager layoutManager) {
        this.mRecyclerView = recyclerView;
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (mRecyclerView != null && layoutManager.getItemCount() > 0) {
            int last = layoutManager.findLastVisibleItemPosition();
            if (listSize() > 0 && last == layoutManager.getItemCount() - 1) {
                int pageNum = (int) Math.ceil(listSize() / 10);
                if (pageNum != currentPage) {
                    currentPage = pageNum;
                    pageNum += 1;
                    loadMoreItems(pageNum);
                }
            }
        }
    }

    protected abstract void loadMoreItems(int pageNum);

    public abstract int listSize();
}
