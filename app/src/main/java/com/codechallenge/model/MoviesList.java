package com.codechallenge.model;

import java.util.List;

public class MoviesList {

    private String Response;

    private String totalResults;

    private String Error;

    private List<Search> Search;

    public String getResponse() {
        return Response;
    }

    public void setResponse(String Response) {
        this.Response = Response;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public List<Search> getSearch() {
        return Search;
    }

    public void setSearch(List<Search> Search) {
        this.Search = Search;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }
}
