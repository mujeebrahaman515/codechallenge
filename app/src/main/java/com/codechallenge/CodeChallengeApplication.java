package com.codechallenge;

import android.app.Application;

import com.codechallenge.di.component.ApplicationComponent;
import com.codechallenge.di.component.DaggerApplicationComponent;
import com.codechallenge.di.modules.ContextModule;
import com.codechallenge.ui.BaseActivity;

public class CodeChallengeApplication extends Application {

    ApplicationComponent applicationComponent;

    public static CodeChallengeApplication get(BaseActivity activity) {
        return (CodeChallengeApplication) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder().contextModule(new ContextModule(this)).build();
        applicationComponent.injectApplication(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}


