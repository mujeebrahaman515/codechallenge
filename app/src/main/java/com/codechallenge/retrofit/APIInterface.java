package com.codechallenge.retrofit;

import com.codechallenge.model.MovieDetails;
import com.codechallenge.model.MoviesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("?apikey=b9bd48a6&type=movie")
    Call<MoviesList> getMoviesList(@Query("s") String searchText, @Query("page") String pageNum);

    @GET("?apikey=b9bd48a6")
    Call<MovieDetails> getMovieDetails(@Query("i") String imbdId);
}
